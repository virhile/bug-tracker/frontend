import 'package:flutter/material.dart';

class ConfirmButton extends StatelessWidget {
  final Widget child;
  final Color color;
  final Function onPressed;

  ConfirmButton({
    Key key,
    @required this.child,
    @required this.color,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: this.color,
        onPressed: this.onPressed,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 12),
          child: this.child,
        ),
      ),
    );
  }
}
