import 'package:flutter/material.dart';

class LabeledInput extends StatelessWidget {
  final String labelText;
  final Color labelColor;
  final TextStyle labelTextStyle;
  final Function onChanged;
  final bool isPassword;

  const LabeledInput({
    Key key,
    @required this.labelText,
    @required this.labelColor,
    @required this.labelTextStyle,
    this.onChanged,
    this.isPassword = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
            color: this.labelColor,
            borderRadius: BorderRadius.all(Radius.circular(2)),
          ),
          child: Padding(
            padding: EdgeInsets.all(4),
            child: Text(this.labelText, style: this.labelTextStyle),
          ),
        ),
        Divider(height: 8),
        Container(
          color: Colors.white,
          child: TextFormField(
            obscureText: this.isPassword,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 8),
            ),
            onChanged: this.onChanged,
          ),
        ),
      ],
    );
  }
}
