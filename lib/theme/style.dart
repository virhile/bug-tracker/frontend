import 'package:flutter/material.dart';

final ThemeData defaultTheme = _buildDefaultTheme();

ThemeData _buildDefaultTheme() {
  final Color _lightPrimaryColor = Color(0xFF393e46);
  final Color _lightPrimaryVariantColor = Color(0xFF10182d);
  final Color _lightOnPrimary = Color(0xFFeeeeee);

  final Color _lightSecondaryColor = Color(0xFF00adb5);
  final Color _lightSecondaryVariantColor = Color(0xFF5cdfe7);
  final Color _lightOnSecondary = Color(0xFF222831);

  final Color _lightBackgroundColor = Color(0xFFeeeeee);
  final Color _lightOnBackground = Color(0xFF001021);

  final Color _lightSurfaceColor = Color(0xFFeeeeee);
  final Color _lightOnSurfaceColor = Color(0xFF111111);

  final Color _lightErrorColor = Color(0xFFeeeeee);
  final Color _lightOnError = Color(0xFFBD1E1E);

  ThemeData themeData = ThemeData(
    primaryColor: _lightPrimaryColor,
    accentColor: _lightSecondaryColor,
    backgroundColor: _lightBackgroundColor,
    brightness: Brightness.light,
    colorScheme: ColorScheme.light(
      primary: _lightPrimaryColor,
      primaryVariant: _lightPrimaryVariantColor,
      secondary: _lightSecondaryColor,
      secondaryVariant: _lightSecondaryVariantColor,
      surface: _lightSurfaceColor,
      background: _lightBackgroundColor,
      error: _lightErrorColor,
      onPrimary: _lightOnPrimary,
      onSecondary: _lightOnSecondary,
      onSurface: _lightOnSurfaceColor,
      onBackground: _lightOnBackground,
      onError: _lightOnError,
      brightness: Brightness.light,
    ),
  );
  return themeData.copyWith(
    primaryTextTheme: themeData.primaryTextTheme.apply(
      bodyColor: _lightOnPrimary,
      displayColor: _lightOnPrimary,
    ),
    accentTextTheme: themeData.accentTextTheme.apply(
      bodyColor: _lightOnSecondary,
      displayColor: _lightOnSecondary,
    ),
  );
}
