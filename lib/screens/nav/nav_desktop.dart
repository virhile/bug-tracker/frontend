import 'package:flutter/material.dart';

import 'package:frontend/components/logo.dart';
import 'package:frontend/screens/nav/nav_item.dart';

class NavDesktop extends StatelessWidget {
  const NavDesktop({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(actions: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Logo(),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              NavItem('Episodes'),
              SizedBox(
                width: 60,
              ),
              NavItem('About'),
            ],
          )
        ],
      ),
    ]);
  }
}
