import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'package:frontend/screens/nav/nav_desktop.dart';
import 'package:frontend/screens/nav/nav_mobile.dart';

class Nav extends StatelessWidget implements PreferredSizeWidget {
  const Nav({Key key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(100);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: NavMobile(),
      tablet: NavDesktop(),
    );
  }
}
