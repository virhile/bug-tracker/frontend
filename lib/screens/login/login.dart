import 'package:flutter/material.dart';

import 'package:frontend/components/logo.dart';
import 'package:frontend/screens/login/create_account/create_account_box.dart';
import 'package:frontend/screens/login/sign_in/sign_in_box.dart';

class Login extends StatelessWidget {
  const Login({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height / 2;
    return Scaffold(
      appBar: AppBar(
        title: Logo(),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SignInBox(height: height),
              CreateAccountBox(height: height),
            ],
          ),
        ),
      ),
    );
  }
}
