import 'package:flutter/material.dart';
import 'package:frontend/screens/login/box_header.dart';
import 'package:frontend/screens/login/create_account/create_account_form.dart';

class CreateAccountBox extends StatelessWidget {
  final double height;

  const CreateAccountBox({
    Key key,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = Theme.of(context).accentColor;
    return Column(
      children: [
        BoxHeader(
          backgroundColor: backgroundColor,
          textStyle: Theme.of(context).accentTextTheme.headline6,
          text: "CREATE ACCOUNT",
        ),
        Divider(height: 8),
        Container(
          width: MediaQuery.of(context).size.width / 2.5,
          height: this.height,
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(8)),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width / 16,
              vertical: 32,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CreateAccountForm(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
