import 'package:flutter/material.dart';
import 'package:frontend/blocs/create_account_bloc.dart';
import 'package:frontend/components/confirm_button.dart';
import 'package:frontend/components/labeled_input.dart';

class CreateAccountForm extends StatefulWidget {
  @override
  CreateAccountFormState createState() {
    return CreateAccountFormState();
  }
}

class CreateAccountFormState extends State<CreateAccountForm> {
  final _formKey = GlobalKey<FormState>();
  CreateAccountBloc createAccountBloc = CreateAccountBloc();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          usernameField(this.createAccountBloc),
          Divider(),
          passwordField(this.createAccountBloc),
          Divider(),
          emailField(this.createAccountBloc),
          Divider(),
          formButton(this.createAccountBloc),
        ],
      ),
    );
  }
}

Widget usernameField(CreateAccountBloc bloc) => StreamBuilder<String>(
      stream: bloc.username,
      builder: (context, snap) {
        final labelColor = Theme.of(context).primaryColor;
        return LabeledInput(
          labelText: "username",
          labelColor: labelColor,
          labelTextStyle: Theme.of(context).primaryTextTheme.bodyText1,
          onChanged: bloc.changeUsername,
        );
      },
    );

Widget passwordField(CreateAccountBloc bloc) => StreamBuilder<String>(
      stream: bloc.password,
      builder: (context, snap) {
        final labelColor = Theme.of(context).primaryColor;
        return LabeledInput(
          labelText: "password",
          labelColor: labelColor,
          labelTextStyle: Theme.of(context).primaryTextTheme.bodyText1,
          onChanged: bloc.changePassword,
          isPassword: true,
        );
      },
    );

Widget emailField(CreateAccountBloc bloc) => StreamBuilder<String>(
      stream: bloc.password,
      builder: (context, snap) {
        final labelColor = Theme.of(context).primaryColor;
        return LabeledInput(
          labelText: "email address",
          labelColor: labelColor,
          labelTextStyle: Theme.of(context).primaryTextTheme.bodyText1,
          onChanged: bloc.changeEmail,
          isPassword: true,
        );
      },
    );

Widget formButton(CreateAccountBloc bloc) => StreamBuilder<bool>(
      stream: bloc.loading,
      builder: (context, snap) {
        final isLoading = (snap.hasData && snap.data);
        final labelColor = Theme.of(context).primaryColor;
        return ConfirmButton(
          child: isLoading
              ? CircularProgressIndicator()
              : Text(
                  "SIGN IN",
                  style: Theme.of(context).primaryTextTheme.button,
                ),
          color: isLoading ? Colors.grey : labelColor,
          onPressed: bloc.submit,
        );
      },
    );
