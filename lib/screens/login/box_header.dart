import 'package:flutter/material.dart';

class BoxHeader extends StatelessWidget {
  final Color backgroundColor;
  final TextStyle textStyle;
  final String text;
  final bool isLeft;

  const BoxHeader({
    Key key,
    @required this.backgroundColor,
    @required this.textStyle,
    @required this.text,
    this.isLeft = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        width: MediaQuery.of(context).size.width / 2.5,
        padding: EdgeInsets.symmetric(horizontal: 64, vertical: 32),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: isLeft
              ? BorderRadius.only(topLeft: Radius.circular(8))
              : BorderRadius.only(topRight: Radius.circular(8)),
          color: this.backgroundColor,
        ),
        child: Text(
          this.text,
          style: this.textStyle,
          overflow: TextOverflow.fade,
        ),
      ),
    );
  }
}
