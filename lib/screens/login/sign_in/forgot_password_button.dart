import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class ForgotPasswordButton extends StatefulWidget {
  const ForgotPasswordButton({
    Key key,
  }) : super(key: key);
  _ForgotPasswordButtonState createState() => _ForgotPasswordButtonState();
}

class _ForgotPasswordButtonState extends State<ForgotPasswordButton> {
  bool _isHovered = false;

  void setHovered(PointerEvent details) {
    setState(() {
      _isHovered = !_isHovered;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: setHovered,
      onExit: setHovered,
      child: RichText(
        text: TextSpan(
          style: TextStyle(
              decoration:
                  _isHovered ? TextDecoration.underline : TextDecoration.none,
              color: Colors.grey.shade700),
          text: "I forgot my password",
          recognizer: TapGestureRecognizer()
            ..onTap = () {
              // TODO implement forgotten password functionality
              print('On forgot password click');
            },
        ),
      ),
    );
  }
}
