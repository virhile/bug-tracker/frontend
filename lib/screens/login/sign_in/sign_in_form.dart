import 'package:flutter/material.dart';
import 'package:frontend/blocs/login_bloc.dart';
import 'package:frontend/components/confirm_button.dart';
import 'package:frontend/components/labeled_input.dart';

class SignInForm extends StatefulWidget {
  @override
  SignInFormState createState() {
    return SignInFormState();
  }
}

class SignInFormState extends State<SignInForm> {
  final _formKey = GlobalKey<FormState>();
  LoginBloc loginBloc = LoginBloc();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          usernameField(this.loginBloc),
          Divider(),
          passwordField(this.loginBloc),
          Divider(),
          formButton(this.loginBloc),
        ],
      ),
    );
  }
}

Widget usernameField(LoginBloc bloc) => StreamBuilder<String>(
      stream: bloc.username,
      builder: (context, snap) {
        final labelColor = Theme.of(context).accentColor;
        return LabeledInput(
          labelText: "username",
          labelColor: labelColor,
          labelTextStyle: Theme.of(context).accentTextTheme.bodyText1,
          onChanged: bloc.changeUsername,
        );
      },
    );

Widget passwordField(LoginBloc bloc) => StreamBuilder<String>(
      stream: bloc.password,
      builder: (context, snap) {
        final labelColor = Theme.of(context).accentColor;
        return LabeledInput(
          labelText: "password",
          labelColor: labelColor,
          labelTextStyle: Theme.of(context).accentTextTheme.bodyText1,
          onChanged: bloc.changeUsername,
          isPassword: true,
        );
      },
    );

Widget formButton(LoginBloc bloc) => StreamBuilder<bool>(
      stream: bloc.loading,
      builder: (context, snap) {
        final isLoading = (snap.hasData && snap.data);
        final labelColor = Theme.of(context).accentColor;
        return ConfirmButton(
          child: isLoading
              ? CircularProgressIndicator()
              : Text(
                  "SIGN IN",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
          color: isLoading ? Colors.grey : labelColor,
          onPressed: bloc.submit,
        );
      },
    );
