import 'package:flutter/material.dart';
import 'package:frontend/screens/login/box_header.dart';
import 'package:frontend/screens/login/sign_in/forgot_password_button.dart';
import 'package:frontend/screens/login/sign_in/sign_in_form.dart';

class SignInBox extends StatelessWidget {
  final double height;

  const SignInBox({
    Key key,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color primaryColor = Theme.of(context).primaryColor;
    return Column(
      children: [
        BoxHeader(
          backgroundColor: primaryColor,
          textStyle: Theme.of(context).primaryTextTheme.headline6,
          text: "SIGN IN TO YOUR ACCOUNT",
          isLeft: true,
        ),
        Divider(height: 8),
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 16,
            vertical: 32,
          ),
          width: MediaQuery.of(context).size.width / 2.5,
          height: this.height,
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: primaryColor,
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SignInForm(),
              Divider(),
              ForgotPasswordButton(),
            ],
          ),
        ),
      ],
    );
  }
}
