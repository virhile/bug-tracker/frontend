import 'package:flutter/material.dart';

import 'package:frontend/blocs/authorization_bloc.dart';
import 'package:frontend/screens/home.dart';
import 'package:frontend/screens/login/login.dart';
import 'package:frontend/theme/style.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    authBloc.restoreSession();

    createContent() {
      return StreamBuilder<bool>(
        stream: authBloc.isSessionValid,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData && snapshot.data) {
            return Home();
          } else {
            return Login();
          }
        },
      );
    }

    return MaterialApp(
      title: 'Bug Tracker',
      theme: defaultTheme,
      home: createContent(),
    );
  }
}
