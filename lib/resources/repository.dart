import 'package:frontend/resources/auth_provider.dart';

class Repository {
  final AuthProvider authProvider = AuthProvider();
  Future<String> login(String username, String password) =>
      authProvider.login(username: username, password: password);
}
