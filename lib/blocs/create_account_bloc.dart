import 'package:rxdart/rxdart.dart';

import 'package:frontend/blocs/authorization_bloc.dart';
import 'package:frontend/resources/repository.dart';

class CreateAccountBloc {
  Repository repository = Repository();

  final BehaviorSubject _usernameController = BehaviorSubject<String>();
  final BehaviorSubject _passwordController = BehaviorSubject<String>();
  final BehaviorSubject _emailController = BehaviorSubject<String>();
  final PublishSubject _loadingData = PublishSubject<bool>();

  Function(String) get changeUsername => _usernameController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;
  Function(String) get changeEmail => _passwordController.sink.add;

  Stream<String> get username => _usernameController.stream.value;
  Stream<String> get password => _passwordController.stream.value;
  Stream<String> get email => _passwordController.stream.value;
  Stream<bool> get loading => _loadingData.stream;

  void register(username, password, email) {}

  void submit() => register(_usernameController.value,
      _passwordController.value, _emailController.value);

  login(String username, String password) async {
    _loadingData.sink.add(true);
    // String token = await repository.login(username, password);
    _loadingData.sink.add(false);
    // authBloc.openSession(token);
  }

  void dispose() {
    _usernameController.close();
    _passwordController.close();
    _emailController.close();
    _loadingData.close();
  }
}
